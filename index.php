<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 01</title>
</head>
<body>
	<h1>Activity 01</h1>

	<h2>Person</h2>
	<p><?php echo $person->printName(); ?></p>

	<h2>Developer</h2>
	<p><?php echo $developer->printName(); ?></p>

	<h2>Engineer</h2>
	<p><?php echo $engineer->printName(); ?></p>

</body>
</html>